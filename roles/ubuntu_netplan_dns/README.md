Ubuntu Netplan DNS
=========

Ansible role to add/replace DNS nameservers in Ubuntu servers with Netplan enabled.

By default, it's adds Cloudfare IPv4 & IPv6 nameservers addresses.

This is specified in the `vars.yml` file. To override it, you should change the following variables:

- `dns1_ipv4`
- `dns2_ipv4`
- `dns1_ipv6`
- `dns2_ipv6`

Also, the boolean `override_dns` variable defines if the role overwrites the existing nameservers or just adds the new ones.


Requirements
------------

### Configure python environment

In order to assure that the playbooks contained here are run always with the same ansible version and linted the same way, we are using [`pyenv>

Follow [Installing and using pyenv](https://github.com/coopdevs/handbook/wiki/Installing-and-using-pyenv), or, in short:

```sh
pyenv install 3.8.12
pyenv virtualenv 3.8.12 dns-netplan
```

### Configure ansible environment

You will need Ansible on your machine to run the playbooks, follow the steps below to install it.

```sh
pyenv exec pip install -r requirements.txt
ansible-galaxy install -r requirements.yml -f
```

### Install pre-commit hooks

We use [pre-commit framework](https://pre-commit.com/) to assure quality code.

```sh
pre-commit install
```

Role Variables
--------------

- `dns1_ipv4`
- `dns2_ipv4`
- `dns1_ipv6`
- `dns2_ipv6`
- `override_dns`

Example Playbook
----------------

 [playbook](/playbooks/change-dns.yml)

License
-------

Fair
